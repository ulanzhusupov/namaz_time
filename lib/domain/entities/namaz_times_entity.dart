import 'package:equatable/equatable.dart';

class NamazTimesEntity extends Equatable {
  final Map<String, dynamic>? timings;
  final String? date;
  final String? timezone;

  const NamazTimesEntity({
    required this.timings,
    required this.date,
    required this.timezone,
  });

  @override
  // TODO: implement props
  List<Object?> get props => [
        timings,
        date,
      ];
}
