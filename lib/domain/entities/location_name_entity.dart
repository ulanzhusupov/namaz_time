import 'package:equatable/equatable.dart';

class LocationNameEntity extends Equatable {
  final String? lang;
  final String? continent;
  final String? countryName;
  final String? countryCode;
  final String? city;

  const LocationNameEntity(
      {required this.lang,
      required this.continent,
      required this.countryName,
      required this.countryCode,
      required this.city});

  @override
  List<Object?> get props => [
        lang,
        continent,
        countryName,
        countryCode,
        city,
      ];
}
