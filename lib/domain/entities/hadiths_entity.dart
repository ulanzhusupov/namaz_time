import 'package:equatable/equatable.dart';

class HadithsEntity extends Equatable {
  final int? id;
  final String? hadithNumber;
  final String? englishNarrator;
  final String? hadithEnglish;
  final String? hadithUrdu;
  final String? urduNarrator;
  final String? hadithArabic;
  final String? headingArabic;
  final String? headingUrdu;
  final String? headingEnglish;
  final String? chapterId;
  final String? bookSlug;
  final String? volume;
  final String? status;

  const HadithsEntity({
    this.id,
    this.hadithNumber,
    this.englishNarrator,
    this.hadithEnglish,
    this.hadithUrdu,
    this.urduNarrator,
    this.hadithArabic,
    this.headingArabic,
    this.headingUrdu,
    this.headingEnglish,
    this.chapterId,
    this.bookSlug,
    this.volume,
    this.status,
  });

  @override
  List<Object?> get props => [
        id,
        hadithNumber,
        englishNarrator,
        hadithEnglish,
        hadithUrdu,
        urduNarrator,
        hadithArabic,
        headingArabic,
        headingUrdu,
        headingEnglish,
        chapterId,
        bookSlug,
        volume,
        status,
      ];
}
