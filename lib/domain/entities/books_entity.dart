import 'package:namaz_time/domain/entities/entity.dart';

class BooksEntity extends Entity {
  final int id;
  final String bookName;
  final String writerName;
  final String writerDeath;
  final String bookSlug;
  final String hadithsCount;
  final String chaptersCount;

  const BooksEntity({
    required this.id,
    required this.bookName,
    required this.writerName,
    required this.writerDeath,
    required this.bookSlug,
    required this.hadithsCount,
    required this.chaptersCount,
  });

  @override
  List<Object?> get props => [
        id,
        bookName,
        writerName,
        writerDeath,
        bookSlug,
        hadithsCount,
        chaptersCount
      ];
}
