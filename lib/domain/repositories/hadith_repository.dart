import 'package:namaz_time/domain/entities/books_entity.dart';
import 'package:namaz_time/domain/entities/chapters_entity.dart';
import 'package:namaz_time/domain/entities/hadiths_entity.dart';

abstract class HadithsRepository {
  Future<List<BooksEntity>?> getBooks();
  Future<List<ChaptersEntity>?> getChapters(String bookSlug);
  Future<List<HadithsEntity>?> getHadithsByChapterAndBook(
      String chapter, String bookSlug);
}
