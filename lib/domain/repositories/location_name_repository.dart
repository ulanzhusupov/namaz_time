import 'package:dartz/dartz.dart';
import 'package:namaz_time/core/error/failure.dart';
import 'package:namaz_time/core/platform/location_info.dart';
import 'package:namaz_time/domain/entities/location_name_entity.dart';

abstract class LocationNameRepository {
  Future<Either<Failure, LocationNameEntity>> getLocationName(
      LocationInfo locationInfo);
}
