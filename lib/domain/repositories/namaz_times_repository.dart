import 'package:dartz/dartz.dart';
import 'package:namaz_time/core/error/failure.dart';
import 'package:namaz_time/core/platform/location_info.dart';
import 'package:namaz_time/domain/entities/namaz_times_entity.dart';

abstract class NamazTimesRepository {
  Future<Either<Failure, List<NamazTimesEntity>>> getTimesForMonth(
      int year, int month, LocationInfo locationInfo);
}
