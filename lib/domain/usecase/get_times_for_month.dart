import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:namaz_time/core/error/failure.dart';
import 'package:namaz_time/core/platform/location_info.dart';
import 'package:namaz_time/core/usecases/usecase.dart';
import 'package:namaz_time/domain/entities/namaz_times_entity.dart';
import 'package:namaz_time/domain/repositories/namaz_times_repository.dart';

class GetTimesForMonth
    extends UseCase<List<NamazTimesEntity>, NamazTimesParams> {
  final NamazTimesRepository namazTimesRepository;

  GetTimesForMonth({required this.namazTimesRepository});

  @override
  Future<Either<Failure, List<NamazTimesEntity>>> call(
      NamazTimesParams params) async {
    return await namazTimesRepository.getTimesForMonth(
        params.year, params.month, params.locationInfo);
  }
}

class NamazTimesParams extends Equatable {
  final int year;
  final int month;
  final LocationInfo locationInfo;

  const NamazTimesParams({
    required this.year,
    required this.month,
    required this.locationInfo,
  });

  @override
  List<Object?> get props => [
        year,
        month,
        locationInfo,
      ];
}
