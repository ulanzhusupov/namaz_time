import 'package:get_it/get_it.dart';
import 'package:namaz_time/domain/entities/books_entity.dart';
import 'package:namaz_time/domain/entities/chapters_entity.dart';
import 'package:namaz_time/domain/entities/hadiths_entity.dart';
import 'package:namaz_time/domain/repositories/hadith_repository.dart';

class HadithUsecase {
  HadithUsecase({required this.hadithsRepository});
  final HadithsRepository hadithsRepository;
  
  List<BooksEntity>? get booksEntity => _booksEntity;
  List<ChaptersEntity>? get chaptersEntity => _chaptersEntity;
  List<HadithsEntity>? get hadithsEntity => _hadithsEntity;

   List<BooksEntity>? _booksEntity;
   List<ChaptersEntity>? _chaptersEntity;
   List<HadithsEntity>? _hadithsEntity;


  Future<void> getChapters(String bookSlug) async {
    final model = await hadithsRepository.getChapters(bookSlug);
    throwIf(model == null, 'error');
    _chaptersEntity = model!;
  }

  Future<void> getBooks() async {
    final model = await hadithsRepository.getBooks();
    // throwIf(model == null, 'error');
    _booksEntity = model!;
  }

  Future<void> getHadithsByChapterAndBook(
    String chapter,
    String bookSlug,
  ) async {
    final model = await hadithsRepository.getHadithsByChapterAndBook(
      chapter,
      bookSlug,
    );
    throwIf(model == null, 'error');
    _hadithsEntity = model!;
  }
}
