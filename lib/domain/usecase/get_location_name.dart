import 'package:dartz/dartz.dart';
import 'package:equatable/equatable.dart';
import 'package:namaz_time/core/error/failure.dart';
import 'package:namaz_time/core/platform/location_info.dart';
import 'package:namaz_time/domain/entities/location_name_entity.dart';
import 'package:namaz_time/domain/repositories/location_name_repository.dart';

import '../../core/usecases/usecase.dart';

class GetLocationName extends UseCase<LocationNameEntity, LocationParams> {
  final LocationNameRepository locationNameRepository;

  GetLocationName({required this.locationNameRepository});

  @override
  Future<Either<Failure, LocationNameEntity>> call(
      LocationParams params) async {
    return await locationNameRepository.getLocationName(params.locationInfo);
  }
}

class LocationParams extends Equatable {
  final LocationInfo locationInfo;

  const LocationParams({
    required this.locationInfo,
  });

  @override
  List<Object?> get props => [
        locationInfo,
      ];
}
