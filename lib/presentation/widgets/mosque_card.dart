import 'package:flutter/material.dart';
import 'package:namaz_time/core/constant/app_images.dart';
import 'package:namaz_time/core/extensions/context_extensions.dart';
import 'package:namaz_time/presentation/theme/app_colors.dart';
import 'package:namaz_time/presentation/theme/app_fonts.dart';

class MosqueCard extends StatelessWidget {
  const MosqueCard({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.centerRight,
      children: [
        Container(
          width: context.getComponentWidthPercent(210),
          height: context.getComponentHeightPercent(210),
          decoration: BoxDecoration(
            color: AppColors.msqsBg.withOpacity(0.6),
            border: Border.all(
              color: Colors.white,
              width: 1,
            ),
            borderRadius: BorderRadius.circular(24),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(16),
                child: Image.asset(
                  AppImages.mosque1,
                  width: context.getComponentWidthPercent(210),
                  height: context.getComponentHeightPercent(130),
                  fit: BoxFit.cover,
                ),
              ),
              Container(
                padding: const EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Masjid Umar Mosque",
                      style:
                          AppFonts.s14W700.copyWith(color: AppColors.boldText),
                    ),
                    SizedBox(
                      height: context.getComponentHeightPercent(4),
                    ),
                    Row(
                      children: [
                        Icon(
                          Icons.fmd_good,
                          color: AppColors.blueAccent,
                          size: context.getComponentWidthPercent(20),
                        ),
                        SizedBox(
                          width: context.getComponentWidthPercent(5),
                        ),
                        Text(
                          "6Q68+7FM, Tullamulla, Saloora,\nJammu and Kashmir, 191201",
                          style: AppFonts.s10W400.copyWith(
                            color: AppColors.lightGrey,
                          ),
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        const Positioned(
          right: 12,
          child: CircleAvatar(
            radius: 19,
            backgroundColor: Colors.white,
            child: CircleAvatar(
              backgroundColor: AppColors.orange,
              radius: 16,
              foregroundColor: Colors.black,
              child: Icon(
                Icons.bookmark_border_outlined,
                color: Colors.white,
                // size: 12,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
