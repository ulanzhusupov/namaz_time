import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:namaz_time/core/extensions/context_extensions.dart';
import 'package:namaz_time/presentation/bloc/location_name_cubit/location_name_cubit.dart';
import 'package:namaz_time/presentation/bloc/location_name_cubit/location_name_state.dart';
import 'package:namaz_time/presentation/theme/app_fonts.dart';
import 'package:prayers_times/prayers_times.dart';
import 'package:shimmer/shimmer.dart';

class NamazTimeCard extends StatelessWidget {
  const NamazTimeCard({
    super.key,
  });

  String getCyrillicPrayer(String prayer) {
    switch (prayer) {
      case "fajr":
        return "Фаджр — предрассветная молитва";
      case "dhuhr":
        return "Зухр — полуденная молитва";
      case "asr":
        return "Аср — послеполуденная молитва";
      case "maghrib":
        return "Магриб — закатная молитва";
      case "isha":
        return "Иша — ночная молитва";
      default:
        return "";
    }
  }

  String getCurrentPrayerTime(PrayerTimes prayerTimes) {
    String prayer = prayerTimes.currentPrayer();
    return "${prayerTimes.timeForPrayer(prayer)?.hour}:${prayerTimes.timeForPrayer(prayer)?.minute}";
  }

  String getNextPrayerTime(PrayerTimes prayerTimes) {
    String prayer = prayerTimes.nextPrayer();
    return "${prayerTimes.timeForPrayer(prayer)?.hour.toString().padLeft(2, '0')}:${prayerTimes.timeForPrayer(prayer)?.minute.toString().padLeft(2, '0')}";
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: context.screenWidth * 1,
      height: 150.0,
      child: BlocBuilder<LocationNameCubit, LocationNameState>(
          builder: (context, state) {
        if (state is LocationNameLoading) {
          return Padding(
            padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
            child: Shimmer.fromColors(
              baseColor: Colors.white.withOpacity(0.3),
              highlightColor: Colors.white.withOpacity(0.1),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(16),
                  color: Colors.white,
                ),
              ),
            ),
          );
        }
        if (state is LocationNameException) {
          return Center(
            child: Text(
              state.message,
              style: const TextStyle(
                color: Colors.white,
                fontSize: 16,
              ),
            ),
          );
        }
        if (state is LocationNameSuccess) {
          PrayerTimes prayerTimes = state.prayerTimes;
          return Container(
            padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
            width: double.infinity,
            height: 150,
            decoration: BoxDecoration(
              color: const Color.fromARGB(255, 53, 40, 0),
              borderRadius: BorderRadius.circular(16),
            ),
            child: Center(
              child: SingleChildScrollView(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      getCyrillicPrayer(prayerTimes.currentPrayer()),
                      style: AppFonts.s14W400.copyWith(color: Colors.white),
                    ),
                    Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Text(getCurrentPrayerTime(prayerTimes),
                            style:
                                AppFonts.s40W900.copyWith(color: Colors.white)),
                      ],
                    ),
                    Text(
                      "Следующая: ${getCyrillicPrayer(prayerTimes.nextPrayer()).split(" ")[0]}",
                      style: AppFonts.s12W500.copyWith(color: Colors.white),
                    ),
                    Text(
                      getNextPrayerTime(prayerTimes),
                      style: AppFonts.s15W900.copyWith(color: Colors.white),
                    ),
                  ],
                ),
              ),
            ),
          );
        }
        return const SizedBox();
      }),
    );
  }
}


// BlocBuilder<NamazTimesBloc, NamazTimesState>(
//           builder: (context, state) {
//             List<NamazTimesEntity> times = [];
//             if (state is NamazTimesLoadingState) {
//               return const Center(
//                 child: CircularProgressIndicator(),
//               );
//             }
//             if (state is NamazTimesException) {
//               return Center(
//                 child: Text(
//                   state.message,
//                   style: const TextStyle(
//                     color: Colors.white,
//                     fontSize: 16,
//                   ),
//                 ),
//               );
//             }
//             if (state is NamazTimesSuccessState) {
//               times = state.namazTimes;
//               return Center(
//                 child: SingleChildScrollView(
//                   child: Column(
//                     children: times.map((day) {
//                       if (day.date == today) {
//                         return Column(
//                           children: [
//                             Text("${day.timings}"),
//                             Text("Today is: ${day.date}"),
//                           ],
//                         );
//                       }
//                       return const SizedBox.shrink();
//                     }).toList(),
//                   ),
//                 ),
//               );
//             }
//             return const SizedBox();
//           },
//         ),