import 'package:flutter/material.dart';
import 'package:namaz_time/core/constant/app_icons.dart';
import 'package:namaz_time/presentation/theme/app_fonts.dart';

class BooksButton extends StatelessWidget {
  const BooksButton({
    super.key,
    required this.authorName,
    required this.onTap,
  });

  final String authorName;
  final Function() onTap;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8),
      child: InkWell(
        onTap: onTap,
        child: Container(
          padding: const EdgeInsets.symmetric(
            vertical: 5,
            horizontal: 20,
          ),
          decoration: BoxDecoration(
            color: Colors.white.withOpacity(0.2),
            border: Border.all(
              color: Colors.white,
            ),
            borderRadius: BorderRadius.circular(24),
          ),
          child: Row(
            children: [
              Image.asset(
                AppIcons.historyBook,
                width: 24,
              ),
              const SizedBox(width: 10),
              Expanded(
                child: Text(
                  authorName,
                  style: AppFonts.s16W700,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              const Icon(Icons.arrow_right_alt_outlined)
            ],
          ),
        ),
      ),
    );
  }
}
