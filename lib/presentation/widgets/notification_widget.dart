import 'package:flutter/material.dart';
import 'package:namaz_time/core/constant/app_icons.dart';
import 'package:namaz_time/core/extensions/context_extensions.dart';

class NotificationWidget extends StatelessWidget {
  const NotificationWidget({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(14),
      decoration: BoxDecoration(
        color: Colors.white.withOpacity(0.5),
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.circular(14),
        border: Border.all(color: Colors.white),
      ),
      child: Image.asset(
        AppIcons.notification,
        width: context.getComponentWidthPercent(20),
      ),
    );
  }
}
