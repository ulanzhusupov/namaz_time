import 'package:flutter/material.dart';
import 'package:namaz_time/core/extensions/context_extensions.dart';
import 'package:namaz_time/presentation/theme/app_colors.dart';
import 'package:namaz_time/presentation/theme/app_fonts.dart';

class SectionCard extends StatelessWidget {
  const SectionCard({
    super.key,
    required this.image,
    required this.cardText,
    required this.onTap,
  });

  final String image;
  final String cardText;
  final Function() onTap;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTap,
      child: SizedBox(
        width: context.getComponentWidthPercent(77),
        child: Column(
          children: [
            Container(
              padding: const EdgeInsets.all(7),
              decoration: const BoxDecoration(
                color: AppColors.lightOrange,
                borderRadius: BorderRadius.all(Radius.circular(8)),
              ),
              child: Image.asset(
                image,
                width: 35,
              ),
            ),
            const SizedBox(height: 6),
            Text(cardText,
                style: AppFonts.s12W400.copyWith(color: AppColors.blackText))
          ],
        ),
      ),
    );
  }
}
