import 'package:flutter/material.dart';
import 'package:namaz_time/core/constant/app_icons.dart';
import 'package:namaz_time/core/extensions/context_extensions.dart';
import 'package:namaz_time/presentation/theme/app_fonts.dart';

class SearchTextField extends StatelessWidget {
  const SearchTextField({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: context.getComponentWidthPercent(220),
      child: TextField(
        textAlignVertical: TextAlignVertical.center,
        decoration: InputDecoration(
          contentPadding: const EdgeInsets.all(10),
          prefix: Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: InkWell(
              onTap: () {},
              child: Image.asset(
                AppIcons.searchFilled,
                width: 24,
              ),
            ),
          ),
          prefixIconConstraints: const BoxConstraints.expand(
            width: 24,
            height: 24,
          ),
          hintText: "Find",
          alignLabelWithHint: true,
          hintStyle: AppFonts.s14W400,
          filled: true,
          fillColor: Colors.white.withOpacity(0.5),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(14),
            borderSide: const BorderSide(
              color: Colors.white,
              width: 1,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(14),
            borderSide: const BorderSide(
              color: Colors.white,
              width: 1,
            ),
          ),
          suffix: InkWell(
            onTap: () {},
            child: Image.asset(
              AppIcons.searchBlack,
              width: 29,
            ),
          ),
        ),
      ),
    );
  }
}
