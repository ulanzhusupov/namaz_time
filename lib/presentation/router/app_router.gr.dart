// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouterGenerator
// **************************************************************************

// ignore_for_file: type=lint
// coverage:ignore-file

part of 'app_router.dart';

abstract class _$AppRouter extends RootStackRouter {
  // ignore: unused_element
  _$AppRouter({super.navigatorKey});

  @override
  final Map<String, PageFactory> pagesMap = {
    BooksRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const BooksPage(),
      );
    },
    ChaptersRoute.name: (routeData) {
      final args = routeData.argsAs<ChaptersRouteArgs>();
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: ChaptersPage(
          key: args.key,
          bookSlug: args.bookSlug,
        ),
      );
    },
    HadithsRoute.name: (routeData) {
      final args = routeData.argsAs<HadithsRouteArgs>();
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: HadithsPage(
          key: args.key,
          chapter: args.chapter,
          bookSlug: args.bookSlug,
        ),
      );
    },
    HomeRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const HomePage(),
      );
    },
    NamazTimesRoute.name: (routeData) {
      return AutoRoutePage<dynamic>(
        routeData: routeData,
        child: const NamazTimesPage(),
      );
    },
  };
}

/// generated route for
/// [BooksPage]
class BooksRoute extends PageRouteInfo<void> {
  const BooksRoute({List<PageRouteInfo>? children})
      : super(
          BooksRoute.name,
          initialChildren: children,
        );

  static const String name = 'BooksRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [ChaptersPage]
class ChaptersRoute extends PageRouteInfo<ChaptersRouteArgs> {
  ChaptersRoute({
    Key? key,
    required String bookSlug,
    List<PageRouteInfo>? children,
  }) : super(
          ChaptersRoute.name,
          args: ChaptersRouteArgs(
            key: key,
            bookSlug: bookSlug,
          ),
          initialChildren: children,
        );

  static const String name = 'ChaptersRoute';

  static const PageInfo<ChaptersRouteArgs> page =
      PageInfo<ChaptersRouteArgs>(name);
}

class ChaptersRouteArgs {
  const ChaptersRouteArgs({
    this.key,
    required this.bookSlug,
  });

  final Key? key;

  final String bookSlug;

  @override
  String toString() {
    return 'ChaptersRouteArgs{key: $key, bookSlug: $bookSlug}';
  }
}

/// generated route for
/// [HadithsPage]
class HadithsRoute extends PageRouteInfo<HadithsRouteArgs> {
  HadithsRoute({
    Key? key,
    required String chapter,
    required String bookSlug,
    List<PageRouteInfo>? children,
  }) : super(
          HadithsRoute.name,
          args: HadithsRouteArgs(
            key: key,
            chapter: chapter,
            bookSlug: bookSlug,
          ),
          initialChildren: children,
        );

  static const String name = 'HadithsRoute';

  static const PageInfo<HadithsRouteArgs> page =
      PageInfo<HadithsRouteArgs>(name);
}

class HadithsRouteArgs {
  const HadithsRouteArgs({
    this.key,
    required this.chapter,
    required this.bookSlug,
  });

  final Key? key;

  final String chapter;

  final String bookSlug;

  @override
  String toString() {
    return 'HadithsRouteArgs{key: $key, chapter: $chapter, bookSlug: $bookSlug}';
  }
}

/// generated route for
/// [HomePage]
class HomeRoute extends PageRouteInfo<void> {
  const HomeRoute({List<PageRouteInfo>? children})
      : super(
          HomeRoute.name,
          initialChildren: children,
        );

  static const String name = 'HomeRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}

/// generated route for
/// [NamazTimesPage]
class NamazTimesRoute extends PageRouteInfo<void> {
  const NamazTimesRoute({List<PageRouteInfo>? children})
      : super(
          NamazTimesRoute.name,
          initialChildren: children,
        );

  static const String name = 'NamazTimesRoute';

  static const PageInfo<void> page = PageInfo<void>(name);
}
