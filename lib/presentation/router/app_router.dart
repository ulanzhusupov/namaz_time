import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:namaz_time/presentation/pages/books_page.dart';
import 'package:namaz_time/presentation/pages/chapters_page.dart';
import 'package:namaz_time/presentation/pages/hadiths_page.dart';
import 'package:namaz_time/presentation/pages/home_page.dart';
import 'package:namaz_time/presentation/pages/namaz_times_page.dart';

part 'app_router.gr.dart';

@AutoRouterConfig()
class AppRouter extends _$AppRouter {
  @override
  List<AutoRoute> get routes => [
        AutoRoute(
          page: HomeRoute.page,
          initial: true,
        ),
        AutoRoute(
          page: NamazTimesRoute.page,
        ),
        AutoRoute(
          page: BooksRoute.page,
        ),
        AutoRoute(
          page: ChaptersRoute.page,
        ),
        AutoRoute(
          page: HadithsRoute.page,
        ),
      ];
}
