import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:namaz_time/domain/entities/books_entity.dart';
import 'package:namaz_time/domain/entities/chapters_entity.dart';
import 'package:namaz_time/domain/entities/hadiths_entity.dart';

import 'package:namaz_time/domain/usecase/hadith_usecase.dart';

part 'hadith_state.dart';

class HadithCubit extends Cubit<HadithState> {
  HadithCubit(this._usecase) : super(HadithInitial());
  final HadithUsecase _usecase;

  Future<void> getBooks() async {
    try {
      emit(HadithLoading());
      await _usecase.getBooks();
      emit(globalState());
    } catch (e) {
      emit(HadithError(error: e));
    }
  }

  Future<void> getChapters(String bookSlug) async {
    try {
      emit(HadithLoading());
      await _usecase.getChapters(bookSlug);
      emit(globalState());
    } catch (e) {
      emit(HadithError(error: e));
    }
  }

  Future<void> getHadithsByChapterAndBook({
    required String chapter,
    required String bookSlug,
  }) async {
    try {
      emit(HadithLoading());
      await _usecase.getHadithsByChapterAndBook(chapter, bookSlug);
      emit(globalState());
    } catch (e) {
      emit(HadithError(error: e));
    }
  }

  HadithSuccess globalState() => HadithSuccess(
        booksEntity: _usecase.booksEntity,
        chaptersEntity: _usecase.chaptersEntity,
        hadithsEntity: _usecase.hadithsEntity,
      );
}
