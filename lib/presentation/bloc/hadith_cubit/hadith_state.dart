part of 'hadith_cubit.dart';

sealed class HadithState extends Equatable {
  const HadithState();

  @override
  List<Object> get props => [];
}

final class HadithInitial extends HadithState {}

final class HadithLoading extends HadithState {}

final class HadithSuccess extends HadithState {
  final List<BooksEntity>? booksEntity;
  final List<ChaptersEntity>? chaptersEntity;
  final List<HadithsEntity>? hadithsEntity;

  const HadithSuccess({
    this.chaptersEntity,
    this.booksEntity,
    this.hadithsEntity,
  });
}

final class HadithError extends HadithState {
  const HadithError({required this.error});

  final dynamic error;
}
