import 'package:equatable/equatable.dart';
import 'package:namaz_time/domain/entities/location_name_entity.dart';
import 'package:prayers_times/prayers_times.dart';

abstract class LocationNameState extends Equatable {
  const LocationNameState();

  @override
  List<Object> get props => [];
}

class LocationNameInitial extends LocationNameState {}

class LocationNameLoading extends LocationNameState {}

class LocationNameSuccess extends LocationNameState {
  final LocationNameEntity locationName;
  final PrayerTimes prayerTimes;

  const LocationNameSuccess(
      {required this.prayerTimes, required this.locationName});
}

class LocationNameException extends LocationNameState {
  final String message;
  const LocationNameException({required this.message});

  @override
  List<Object> get props => [message];
}
