import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:namaz_time/core/constant/app_consts.dart';
import 'package:namaz_time/core/error/failure.dart';
import 'package:namaz_time/core/platform/location_info.dart';
import 'package:namaz_time/domain/usecase/get_location_name.dart';
import 'package:namaz_time/presentation/bloc/location_name_cubit/location_name_state.dart';
import 'package:prayers_times/prayers_times.dart';

class LocationNameCubit extends Cubit<LocationNameState> {
  final GetLocationName locationNameUseCase;
  LocationNameCubit({required this.locationNameUseCase})
      : super(LocationNameInitial());

  Future<void> getLocationName(LocationInfo locationInfo) async {
    emit(LocationNameLoading());
    final failureOrLocation = await locationNameUseCase
        .call(LocationParams(locationInfo: locationInfo));
    failureOrLocation.fold(
      (error) => emit(
        LocationNameException(message: _mapFailureToMessage(error)),
      ),
      (locationName) {
        Coordinates coordinates =
            Coordinates(locationInfo.latitude, locationInfo.longitude);

        // Specify the calculation parameters for prayer times
        PrayerCalculationParameters params = PrayerCalculationMethod.karachi();
        params.madhab = PrayerMadhab.hanafi;

        // Create a PrayerTimes instance for the specified location
        PrayerTimes prayerTimes = PrayerTimes(
          coordinates: coordinates,
          calculationParameters: params,
          precision: true,
          locationName: "${locationName.continent}/${locationName.city}",
        );
        emit(LocationNameSuccess(
          locationName: locationName,
          prayerTimes: prayerTimes,
        ));
      },
    );
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return AppConsts.SERVER_FAILURE_MESSAGE;
      case CacheFailure:
        return AppConsts.CACHE_FAILURE_MESSAGE;
      default:
        return 'Unexpected Error';
    }
  }
}
