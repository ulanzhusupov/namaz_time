part of 'namaz_times_bloc.dart';

abstract class NamazTimesState extends Equatable {
  const NamazTimesState();

  @override
  List<Object> get props => [];
}

class NamazTimesInitialState extends NamazTimesState {}

class NamazTimesLoadingState extends NamazTimesState {}

class NamazTimesSuccessState extends NamazTimesState {
  final List<NamazTimesEntity> namazTimes;

  const NamazTimesSuccessState({required this.namazTimes});
}

class NamazTimesException extends NamazTimesState {
  final String message;
  const NamazTimesException({required this.message});

  @override
  List<Object> get props => [message];
}
