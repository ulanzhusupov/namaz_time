import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:namaz_time/core/constant/app_consts.dart';
import 'package:namaz_time/core/error/failure.dart';
import 'package:namaz_time/core/platform/location_info.dart';
import 'package:namaz_time/domain/entities/namaz_times_entity.dart';
import 'package:namaz_time/domain/usecase/get_times_for_month.dart';

part 'namaz_times_event.dart';
part 'namaz_times_state.dart';

class NamazTimesBloc extends Bloc<NamazTimesEvent, NamazTimesState> {
  final GetTimesForMonth getTimesForMonth;
  NamazTimesBloc({required this.getTimesForMonth})
      : super(NamazTimesInitialState()) {
    on<GetNamazTimesEvent>(_exampleFunc);
  }

  Future<void> _exampleFunc(
    GetNamazTimesEvent event,
    Emitter<NamazTimesState> emit,
  ) async {
    emit(NamazTimesLoadingState());

    final failureOrTimes = await getTimesForMonth.call(NamazTimesParams(
        year: event.year,
        month: event.month,
        locationInfo: event.locationInfo));
    failureOrTimes.fold(
      (error) => emit(
        NamazTimesException(message: _mapFailureToMessage(error)),
      ),
      (times) => emit(
        NamazTimesSuccessState(namazTimes: times),
      ),
    );
  }

  String _mapFailureToMessage(Failure failure) {
    switch (failure.runtimeType) {
      case ServerFailure:
        return AppConsts.SERVER_FAILURE_MESSAGE;
      case CacheFailure:
        return AppConsts.CACHE_FAILURE_MESSAGE;
      default:
        return 'Unexpected Error';
    }
  }
}
