part of 'namaz_times_bloc.dart';

abstract class NamazTimesEvent extends Equatable {
  const NamazTimesEvent();

  @override
  List<Object> get props => [];
}

class GetNamazTimesEvent extends NamazTimesEvent {
  final int year;
  final int month;
  final LocationInfo locationInfo;

  const GetNamazTimesEvent({
    required this.year,
    required this.month,
    required this.locationInfo,
  });
}
