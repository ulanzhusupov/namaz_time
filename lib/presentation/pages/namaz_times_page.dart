import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:namaz_time/core/constant/app_icons.dart';
import 'package:namaz_time/core/constant/app_images.dart';
import 'package:namaz_time/core/extensions/context_extensions.dart';
import 'package:namaz_time/core/platform/location_info.dart';
import 'package:namaz_time/core/platform/current_location_service.dart';
import 'package:namaz_time/presentation/bloc/location_name_cubit/location_name_cubit.dart';
import 'package:namaz_time/presentation/bloc/location_name_cubit/location_name_state.dart';
import 'package:namaz_time/presentation/router/app_router.dart';
import 'package:namaz_time/presentation/theme/app_colors.dart';
import 'package:namaz_time/presentation/theme/app_fonts.dart';
import 'package:namaz_time/presentation/widgets/mosque_card.dart';
import 'package:namaz_time/presentation/widgets/namaz_time_card.dart';
import 'package:namaz_time/presentation/widgets/notification_widget.dart';
import 'package:namaz_time/presentation/widgets/search_textfield.dart';
import 'package:namaz_time/presentation/widgets/section_card.dart';
import 'package:shimmer/shimmer.dart';

@RoutePage()
class NamazTimesPage extends StatefulWidget {
  const NamazTimesPage({super.key});
  @override
  State<NamazTimesPage> createState() => _NamazTimesPageState();
}

class _NamazTimesPageState extends State<NamazTimesPage> {
  // late LocationInfo locationInfo;

  @override
  void initState() {
    super.initState();
    CurrentLocationService.initPermission();
    getCurrentLocation();
  }

  getCurrentLocation() async {
    LocationInfo locationInfo =
        await CurrentLocationService.fetchCurrentLocation();
    BlocProvider.of<LocationNameCubit>(context).getLocationName(locationInfo);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Container(
        width: context.screenWidth,
        height: context.screenHeight,
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage(AppImages.bg),
            fit: BoxFit.cover,
          ),
        ),
        child: SingleChildScrollView(
          padding: const EdgeInsets.all(28),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(height: context.getComponentHeightPercent(28)),
              Row(
                children: [
                  const SearchTextField(),
                  SizedBox(width: context.getComponentWidthPercent(21)),
                  const NotificationWidget(),
                ],
              ),
              const SizedBox(height: 9),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    "Местоположение",
                    style: AppFonts.s12W700I,
                  ),
                  BlocBuilder<LocationNameCubit, LocationNameState>(
                    builder: (context, state) {
                      if (state is LocationNameLoading) {
                        return SizedBox(
                          width: context.screenWidth * 0.4,
                          height: 10,
                          child: Shimmer.fromColors(
                            baseColor: Colors.white.withOpacity(0.3),
                            highlightColor: Colors.white.withOpacity(0.1),
                            child: Container(
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(16),
                                color: Colors.white,
                              ),
                            ),
                          ),
                        );
                        // return Text("Загрузка...",
                        //     style: AppFonts.s13W900I
                        //         .copyWith(color: AppColors.blueAccent));
                      }
                      if (state is LocationNameException) {
                        return Text(state.message,
                            style:
                                AppFonts.s10W400.copyWith(color: Colors.white));
                      }
                      if (state is LocationNameSuccess) {
                        return Text(
                          "${state.locationName.countryName}/${state.locationName.city}",
                          style: AppFonts.s13W900I
                              .copyWith(color: AppColors.blueAccent),
                        );
                      }
                      return const SizedBox();
                    },
                  ),
                ],
              ),
              const SizedBox(height: 26),
              const NamazTimeCard(),
              const SizedBox(height: 20),
              Container(
                padding: const EdgeInsets.symmetric(
                  vertical: 20,
                ),
                decoration: BoxDecoration(
                  color: Colors.white.withOpacity(0.2),
                  border: Border.all(
                    color: Colors.white,
                  ),
                  borderRadius: BorderRadius.circular(24),
                ),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        SectionCard(
                          onTap: () => context.router.push(const BooksRoute()),
                          image: AppIcons.man,
                          cardText: "Hadith",
                        ),
                        SectionCard(
                          onTap: () {},
                          image: AppIcons.dua,
                          cardText: "Dua",
                        ),
                        SectionCard(
                          onTap: () {},
                          image: AppIcons.quran,
                          cardText: "Daily Verse",
                        ),
                      ],
                    ),
                    const SizedBox(height: 24),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        SectionCard(
                          onTap: () {},
                          image: AppIcons.partners,
                          cardText: "Community",
                        ),
                        SectionCard(
                          onTap: () {},
                          image: AppIcons.map,
                          cardText: "Maps",
                        ),
                        SectionCard(
                          onTap: () {},
                          image: AppIcons.category,
                          cardText: "More",
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 14),
              Text(
                "Mosques",
                style: AppFonts.s16W700.copyWith(color: AppColors.boldText),
              ),
              SizedBox(height: context.getComponentHeightPercent(17)),
              const MosqueCard(),
            ],
          ),
        ),
      ),
    );
  }
}
