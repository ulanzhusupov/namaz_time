import 'dart:developer';
import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:namaz_time/core/constant/app_images.dart';
import 'package:namaz_time/core/extensions/context_extensions.dart';
import 'package:namaz_time/presentation/bloc/hadith_cubit/hadith_cubit.dart';
import 'package:namaz_time/presentation/router/app_router.dart';
import 'package:namaz_time/presentation/theme/app_fonts.dart';
import 'package:namaz_time/presentation/widgets/books_button.dart';
import 'package:shimmer/shimmer.dart';

@RoutePage()
class BooksPage extends StatefulWidget {
  const BooksPage({super.key});

  @override
  State<BooksPage> createState() => _HadithPageState();
}

class _HadithPageState extends State<BooksPage> {
  @override
  void initState() {
    super.initState();
    BlocProvider.of<HadithCubit>(context).getBooks();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: context.screenWidth,
        height: context.screenHeight,
        decoration: const BoxDecoration(
          color: Colors.black,
          image: DecorationImage(
            image: AssetImage(AppImages.bg),
            fit: BoxFit.cover,
          ),
        ),
        child: SafeArea(
          child: SingleChildScrollView(
            padding: const EdgeInsets.symmetric(horizontal: 28),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: context.getComponentHeightPercent(28)),
                const Text(
                  "Books",
                  style: AppFonts.s40W900,
                ),
                SizedBox(height: context.getComponentHeightPercent(28)),
                BlocBuilder<HadithCubit, HadithState>(
                  // buildWhen: (previous, current) => current is HadithSuccess,
                  builder: (context, state) {
                    log('data-unique: state: ${state} ');
                    if (state is HadithLoading) {
                      return SizedBox(
                        height: context.screenHeight * 0.7,
                        child: ListView.builder(
                          itemCount: 9,
                          itemBuilder: (context, index) {
                            return Padding(
                              padding: const EdgeInsets.symmetric(vertical: 8),
                              child: SizedBox(
                                width: double.infinity,
                                height: 30,
                                child: Shimmer.fromColors(
                                  baseColor: Colors.white.withOpacity(0.3),
                                  highlightColor: Colors.white.withOpacity(0.1),
                                  child: Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(16),
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                      );
                    }

                    if (state is HadithError) {
                      return const Text(
                        "Произошла ошибка",
                        style: AppFonts.s14W400,
                      );
                    }

                    if (state is HadithSuccess) {
                      return Column(
                        children: state.booksEntity
                                ?.map((e) => BooksButton(
                                      onTap: () {
                                        context.router.push(ChaptersRoute(
                                            bookSlug: e.bookSlug));
                                      },
                                      authorName: e.bookName,
                                    ))
                                .toList() ??
                            [],
                      );
                    }

                    return const SizedBox();
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
