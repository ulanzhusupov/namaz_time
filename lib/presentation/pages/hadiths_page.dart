import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:namaz_time/core/constant/app_images.dart';
import 'package:namaz_time/core/extensions/context_extensions.dart';
import 'package:namaz_time/domain/entities/hadiths_entity.dart';
import 'package:namaz_time/presentation/bloc/hadith_cubit/hadith_cubit.dart';
import 'package:namaz_time/presentation/theme/app_colors.dart';
import 'package:namaz_time/presentation/theme/app_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shimmer/shimmer.dart';

@RoutePage()
class HadithsPage extends StatefulWidget {
  const HadithsPage({
    super.key,
    required this.chapter,
    required this.bookSlug,
  });
  final String chapter;
  final String bookSlug;

  @override
  State<HadithsPage> createState() => _HadithsPageState();
}

class _HadithsPageState extends State<HadithsPage> {
  // late SharedPreferences sharedPreferences;

  @override
  void initState() {
    super.initState();
    BlocProvider.of<HadithCubit>(context).getHadithsByChapterAndBook(
      chapter: widget.chapter,
      bookSlug: widget.bookSlug,
    );
    // sharedPreferences = SharedPreferences.getInstance();
  }

  // void addToFavorites(HadithsEntity hadith) {
  //   sharedPreferences.getStringList(key)
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Container(
        width: context.screenWidth,
        height: context.screenHeight,
        decoration: const BoxDecoration(
          color: Colors.black,
          image: DecorationImage(
            image: AssetImage(AppImages.mosque2),
            fit: BoxFit.cover,
          ),
        ),
        child: SizedBox(
          height: context.screenHeight,
          child: SingleChildScrollView(
            padding: const EdgeInsets.symmetric(horizontal: 28),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: context.getComponentHeightPercent(28)),
                BlocBuilder<HadithCubit, HadithState>(
                  builder: (context, state) {
                    if (state is HadithLoading) {
                      return SizedBox(
                        height: context.screenHeight,
                        child: ListView.builder(
                          itemCount: 9,
                          itemBuilder: (context, index) {
                            return Padding(
                              padding: const EdgeInsets.symmetric(vertical: 8),
                              child: SizedBox(
                                width: double.infinity,
                                height: 231,
                                child: Shimmer.fromColors(
                                  baseColor: Colors.white.withOpacity(0.3),
                                  highlightColor: Colors.white.withOpacity(0.1),
                                  child: Container(
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(16),
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              ),
                            );
                          },
                        ),
                      );
                    }

                    if (state is HadithError) {
                      return const Text(
                        "Произошла ошибка",
                        style: AppFonts.s14W400,
                      );
                    }

                    if (state is HadithSuccess) {
                      return Column(
                        children: state.hadithsEntity?.map(
                              (e) {
                                return Padding(
                                  padding:
                                      const EdgeInsets.symmetric(vertical: 8),
                                  child: Container(
                                    width: double.infinity,
                                    padding: const EdgeInsets.symmetric(
                                      vertical: 15,
                                      horizontal: 20,
                                    ),
                                    decoration: BoxDecoration(
                                      color: AppColors.msqsBg.withOpacity(0.8),
                                      borderRadius: BorderRadius.circular(16),
                                    ),
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          e.englishNarrator ?? '',
                                          style: AppFonts.s15W700,
                                        ),
                                        const SizedBox(height: 26),
                                        Text(
                                          e.hadithEnglish ?? '',
                                          style: AppFonts.s12W500,
                                        ),
                                        const SizedBox(height: 26),
                                        const Row(
                                          children: [
                                            Icon(
                                              Icons.favorite_outline_outlined,
                                              size: 24,
                                              color: AppColors.blackText,
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                );
                              },
                            ).toList() ??
                            [],
                      );
                    }

                    return const SizedBox();
                  },
                ),
                SizedBox(height: context.getComponentHeightPercent(28)),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
