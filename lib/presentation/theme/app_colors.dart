import 'package:flutter/material.dart';

abstract class AppColors {
  static const Color blueAccent = Color(0xff42A8C3);
  static const Color orange = Color(0xffFFAC84);
  static const Color lightOrange = Color(0xffFFF4EE);
  static const Color blackText = Color(0xff515151);
  static const Color boldText = Color(0xff333333);
  static const Color msqsBg = Color(0xffF0F0F0);
  static const Color lightGrey = Color(0xff8f8f8f);
  static const Color inactiveIcons = Color(0xffD6D6D6);
}
