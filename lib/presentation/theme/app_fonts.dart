import 'package:flutter/material.dart';

abstract class AppFonts {
  static const TextStyle s13W900I = TextStyle(
    fontSize: 13,
    fontWeight: FontWeight.w900,
    fontStyle: FontStyle.italic,
  );
  static const TextStyle s12W700I = TextStyle(
    fontSize: 12,
    fontWeight: FontWeight.w700,
    fontStyle: FontStyle.italic,
  );
  static const TextStyle s12W500 = TextStyle(
    fontSize: 12,
    fontWeight: FontWeight.w500,
    fontStyle: FontStyle.normal,
  );
  static const TextStyle s40W900 = TextStyle(
    fontSize: 40,
    fontWeight: FontWeight.w900,
    fontStyle: FontStyle.normal,
  );
  static const TextStyle s15W900 = TextStyle(
    fontSize: 15,
    fontWeight: FontWeight.w900,
    fontStyle: FontStyle.normal,
  );
  static const TextStyle s14W700 = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.w900,
    fontStyle: FontStyle.normal,
  );
  static const TextStyle s14W400 = TextStyle(
    fontSize: 14,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
  );
  static const TextStyle s12W400 = TextStyle(
    fontSize: 12,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
  );
  static const TextStyle s16W700 = TextStyle(
    fontSize: 16,
    fontWeight: FontWeight.w700,
    fontStyle: FontStyle.normal,
  );
  static const TextStyle s10W700 = TextStyle(
    fontSize: 10,
    fontWeight: FontWeight.w700,
    fontStyle: FontStyle.normal,
  );
  static const TextStyle s10W400 = TextStyle(
    fontSize: 10,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
  );
  static const TextStyle s8W400 = TextStyle(
    fontSize: 8,
    fontWeight: FontWeight.w400,
    fontStyle: FontStyle.normal,
  );
  static const TextStyle s15W700 = TextStyle(
    fontSize: 15,
    fontWeight: FontWeight.w700,
    fontStyle: FontStyle.normal,
  );
}
