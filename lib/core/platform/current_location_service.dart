import 'package:namaz_time/core/platform/location_info.dart';
import 'package:namaz_time/core/platform/location_service.dart';

final class CurrentLocationService {
  static Future<void> initPermission() async {
    if (!await LocationService().checkPermission()) {
      await LocationService().requestPermission();
    }
  }

  static Future<LocationInfo> fetchCurrentLocation() async {
    try {
      return await LocationService().getCurrentLocation();
    } catch (e) {
      return LocationInfo(latitude: 0, longitude: 0);
    }
  }
}
