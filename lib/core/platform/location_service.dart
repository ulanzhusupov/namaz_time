import 'package:geolocator/geolocator.dart';
import 'package:namaz_time/core/platform/location_info.dart';

abstract class AppLocation {
  Future<LocationInfo> getCurrentLocation();

  Future<bool> requestPermission();

  Future<bool> checkPermission();
}

class LocationService implements AppLocation {
  @override
  Future<bool> checkPermission() {
    return Geolocator.checkPermission()
        .then((value) =>
            value == LocationPermission.always ||
            value == LocationPermission.whileInUse)
        .catchError((_) => false);
  }

  @override
  Future<LocationInfo> getCurrentLocation() async {
    return Geolocator.getCurrentPosition().then((value) {
      return LocationInfo(latitude: value.latitude, longitude: value.longitude);
    });
  }

  @override
  Future<bool> requestPermission() {
    return Geolocator.requestPermission()
        .then((value) =>
            value == LocationPermission.always ||
            value == LocationPermission.whileInUse)
        .catchError((_) => false);
  }
}
