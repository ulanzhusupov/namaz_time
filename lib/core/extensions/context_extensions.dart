import 'package:flutter/widgets.dart';

extension ContextExtensions on BuildContext {
  double get screenWidth => MediaQuery.of(this).size.width;
  double get screenHeight => MediaQuery.of(this).size.height;

  double getComponentWidthPercent(double containerWidth) {
    double percent = containerWidth / screenWidth;
    return screenWidth * percent;
  }

  double getComponentHeightPercent(double containerHeight) {
    double percent = containerHeight / screenHeight;
    return screenHeight * percent;
  }
}
