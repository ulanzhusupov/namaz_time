// **************************************************************************
// * AssetsGenerator - Simpler FLutter Generator Extension -
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND


class AppImages {
  AppImages._();
  
  static const String bgOrange = 'assets/images/bg-orange.png';  
  static const String bg = 'assets/images/bg.png';  
  static const String mosque1 = 'assets/images/mosque1.jpg';  
  static const String mosque2 = 'assets/images/mosque2.jpg';  
  static const String splashImage = 'assets/images/splash_image.jpg';  
}
