abstract class AppConsts {
  static const String CACHED_NAMAZ_TIMES_LIST = "CACHED_NAMAZ_TIMES_LIST";
  static const String SERVER_FAILURE_MESSAGE = "SERVER FAILURE";
  static const String CACHE_FAILURE_MESSAGE = "CACHE FAILURE";

  static const String CACHED_BOOKS = "CACHED_BOOKS";
  static const String CACHED_CHAPTERS = "CACHED_CHAPTERS";
  static const String CACHED_HADITHS = "CACHED_HADITHS";
}
