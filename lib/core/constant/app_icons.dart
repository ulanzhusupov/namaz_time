// **************************************************************************
// * AssetsGenerator - Simpler FLutter Generator Extension -
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND


class AppIcons {
  AppIcons._();
  
  static const String category = 'assets/png/Category.png';  
  static const String dua = 'assets/png/dua.png';  
  static const String historyBook = 'assets/png/history-book.png';  
  static const String man = 'assets/png/man.png';  
  static const String map = 'assets/png/map.png';  
  static const String notification = 'assets/png/Notification.png';  
  static const String partners = 'assets/png/partners.png';  
  static const String quran = 'assets/png/quran.png';  
  static const String searchBlack = 'assets/png/SearchBlack.png';  
  static const String searchFilled = 'assets/png/SearchFilled.png';  
}
