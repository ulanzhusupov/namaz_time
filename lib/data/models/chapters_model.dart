import 'package:namaz_time/domain/entities/chapters_entity.dart';

class ChaptersModel extends ChaptersEntity {
  const ChaptersModel({
    required super.id,
    required super.chapterNumber,
    required super.chapterEnglish,
    required super.chapterUrdu,
    required super.chapterArabic,
    required super.bookSlug,
  });

  factory ChaptersModel.fromJson(Map<String, dynamic> json) {
    return ChaptersModel(
        id: json['id'],
        chapterNumber: json['chapterNumber'],
        chapterEnglish: json['chapterEnglish'],
        chapterUrdu: json['chapterUrdu'],
        chapterArabic: json['chapterArabic'],
        bookSlug: json['bookSlug']);
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'chapterNumber': chapterNumber,
      'chapterEnglish': chapterEnglish,
      'chapterUrdu': chapterUrdu,
      'chapterArabic': chapterArabic,
      'bookSlug': bookSlug,
    };
  }
}
