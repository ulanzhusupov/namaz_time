import 'package:namaz_time/domain/entities/location_name_entity.dart';

class LocationNameModel extends LocationNameEntity {
  const LocationNameModel(
      {required super.lang,
      required super.continent,
      required super.countryName,
      required super.countryCode,
      required super.city});

  factory LocationNameModel.fromJson(Map<String, dynamic> json) {
    return LocationNameModel(
      lang: json['lang'],
      continent: json['continent'],
      countryName: json['countryName'],
      countryCode: json['countryCode'],
      city: json['city'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'lang': lang,
      'continent': continent,
      'countryName': countryName,
      'countryCode': countryCode,
      'city': city,
    };
  }
}
