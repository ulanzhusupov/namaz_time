import 'package:namaz_time/domain/entities/books_entity.dart';

class BooksModel extends BooksEntity {
  const BooksModel({
    required super.id,
    required super.bookName,
    required super.writerName,
    required super.writerDeath,
    required super.bookSlug,
    required super.hadithsCount,
    required super.chaptersCount,
  });

  factory BooksModel.fromJson(Map<String, dynamic> json) {
    return BooksModel(
      id: json['id'],
      bookName: json['bookName'],
      writerName: json['writerName'],
      writerDeath: json['writerDeath'],
      bookSlug: json['bookSlug'],
      hadithsCount: json['hadiths_count'],
      chaptersCount: json['chapters_count'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'bookName': bookName,
      'writerName': writerName,
      'writerDeath': writerDeath,
      'bookSlug': bookSlug,
      'hadiths_count': hadithsCount,
      'chapters_count': chaptersCount,
    };
  }
}
