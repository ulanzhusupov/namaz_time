import 'package:namaz_time/domain/entities/namaz_times_entity.dart';

class NamazTimeModel extends NamazTimesEntity {
  const NamazTimeModel(
      {required super.timings, required super.date, required super.timezone});

  factory NamazTimeModel.fromJson(Map<String, dynamic> json) {
    print("JSOOOOOOOOOOn: $json");
    return NamazTimeModel(
        timings: (json['timings'] as Map<String, dynamic>),
        date: json['date']['gregorian']['date'],
        timezone: json['meta']['timezone']);
  }

  Map<String, dynamic> toJson() {
    return {
      'timings': timings,
      'date': date,
    };
  }
}
