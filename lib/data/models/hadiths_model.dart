import 'package:namaz_time/domain/entities/hadiths_entity.dart';

class HadithsModel extends HadithsEntity {
  const HadithsModel({
     super.id,
     super.hadithNumber,
     super.englishNarrator,
     super.hadithEnglish,
     super.hadithUrdu,
     super.urduNarrator,
     super.hadithArabic,
     super.headingArabic,
     super.headingUrdu,
     super.headingEnglish,
     super.chapterId,
     super.bookSlug,
     super.volume,
     super.status,
  });

  factory HadithsModel.fromJson(Map<String, dynamic> json) {
    return HadithsModel(
      id: json['id'],
      hadithNumber: json['hadithNumber'],
      englishNarrator: json['englishNarrator'],
      hadithEnglish: json['hadithEnglish'],
      hadithUrdu: json['hadithUrdu'],
      urduNarrator: json['urduNarrator'],
      hadithArabic: json['hadithArabic'],
      headingArabic: json['headingArabic'],
      headingUrdu: json['headingUrdu'],
      headingEnglish: json['headingEnglish'],
      chapterId: json['chapterId'],
      bookSlug: json['bookSlug'],
      volume: json['volume'],
      status: json['status'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'hadithNumber': hadithNumber,
      'englishNarrator': englishNarrator,
      'hadithEnglish': hadithEnglish,
      'hadithUrdu': hadithUrdu,
      'urduNarrator': urduNarrator,
      'hadithArabic': hadithArabic,
      'headingArabic': headingArabic,
      'headingUrdu': headingUrdu,
      'headingEnglish': headingEnglish,
      'chapterId': chapterId,
      'bookSlug': bookSlug,
      'volume': volume,
      'status': status,
    };
  }
}
