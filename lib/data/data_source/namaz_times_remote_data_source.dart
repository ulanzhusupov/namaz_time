import 'package:dio/dio.dart';
import 'package:namaz_time/core/network/dio_settings.dart';
import 'package:namaz_time/core/platform/location_info.dart';
import 'package:namaz_time/data/models/namaz_times_model.dart';

abstract class NamazTimesRemoteDataSource {
  Future<List<NamazTimeModel>> getTimesForMonth(
      int year, int month, LocationInfo locationInfo);
}

class NamazTimesRemoteDataSourceImpl implements NamazTimesRemoteDataSource {
  final DioSettings dioSettings;

  NamazTimesRemoteDataSourceImpl({required this.dioSettings});

  @override
  Future<List<NamazTimeModel>> getTimesForMonth(
    int year,
    int month,
    LocationInfo locationInfo,
  ) async {
    final response = await dioSettings.dio.get(
        "http://api.aladhan.com/v1/calendar/$year/$month?latitude=${locationInfo.latitude}&longitude=${locationInfo.longitude}&method=2",
        options: Options(
          headers: {'Content-Type': 'application/json'},
        ));

    print("HELLLOOOOOO: ${response.data['data']}");
    final days = response.data['data'];
    print(days.runtimeType);
    List<NamazTimeModel> oyoy =
        (days as List).map((day) => NamazTimeModel.fromJson(day)).toList();
    print("OooooooooooyOyyyyyyyyyy: $oyoy");
    return oyoy;
  }
}
