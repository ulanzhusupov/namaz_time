import 'dart:convert';

import 'package:namaz_time/core/constant/app_consts.dart';
import 'package:namaz_time/core/error/exception.dart';
import 'package:namaz_time/data/models/namaz_times_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class NamazTimesLocalDataSource {
  Future<List<NamazTimeModel>> getLastNamazTimesFromCache();
  Future<void> namazTimesToCache(List<NamazTimeModel> times);
}

class NamazTimesLocalDataSourceImpl implements NamazTimesLocalDataSource {
  final SharedPreferences sharedPreferences;

  NamazTimesLocalDataSourceImpl({required this.sharedPreferences});

  @override
  Future<List<NamazTimeModel>> getLastNamazTimesFromCache() {
    final jsonPersonList =
        sharedPreferences.getStringList(AppConsts.CACHED_NAMAZ_TIMES_LIST);
    if (jsonPersonList?.isNotEmpty ?? false) {
      return Future.value(jsonPersonList
          ?.map((day) => NamazTimeModel.fromJson(jsonDecode(day)))
          .toList());
    } else {
      throw CacheException();
    }
  }

  @override
  Future<void> namazTimesToCache(List<NamazTimeModel> times) {
    final List<String> jsonTimesList =
        times.map((day) => jsonEncode(day.toJson())).toList();

    sharedPreferences.setStringList(
        AppConsts.CACHED_NAMAZ_TIMES_LIST, jsonTimesList);
    print("Writin times to cache");
    return Future.value(jsonTimesList);
  }
}
