import 'package:dio/dio.dart';
import 'package:namaz_time/core/network/dio_settings.dart';
import 'package:namaz_time/core/platform/location_info.dart';
import 'package:namaz_time/data/models/location_name_model.dart';

abstract class LocationNameRemoteDataSource {
  Future<LocationNameModel> getLocationName(LocationInfo locationInfo);
}

class LocationNameRemoteDataSourceImpl implements LocationNameRemoteDataSource {
  final DioSettings dioSettings;

  LocationNameRemoteDataSourceImpl({required this.dioSettings});

  @override
  Future<LocationNameModel> getLocationName(
    LocationInfo locationInfo,
  ) async {
    final response = await dioSettings.dio.get(
        "https://api.bigdatacloud.net/data/reverse-geocode-client?latitude=${locationInfo.latitude}&longitude=${locationInfo.longitude}&localityLanguage=en",
        options: Options(
          headers: {'Content-Type': 'application/json'},
        ));
    final data = response.data;
    return LocationNameModel.fromJson(data);
  }
}
