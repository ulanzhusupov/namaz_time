import 'package:dio/dio.dart';
import 'package:namaz_time/core/constant/api_keys.dart';
import 'package:namaz_time/core/network/dio_settings.dart';
import 'package:namaz_time/data/models/books_model.dart';
import 'package:namaz_time/data/models/chapters_model.dart';
import 'package:namaz_time/data/models/hadiths_model.dart';

abstract class HadithRemoteDataSource {
  Future<List<BooksModel>> getBook();
  Future<List<ChaptersModel>> getChapters(String bookSlug);
  Future<List<HadithsModel>> getHadithsByChapterAndBook(
      String chapter, String bookSlug);
}

class HadithRemoteDataSourceImpl implements HadithRemoteDataSource {
  final DioSettings dioSettings;

  HadithRemoteDataSourceImpl({required this.dioSettings});

  @override
  Future<List<BooksModel>> getBook() async {
    final response = await dioSettings.dio.get(
      "https://www.hadithapi.com/api/books?apiKey=${ApiKeys.HADITH_API}",
      options: Options(
        headers: {'Content-Type': 'application/json'},
      ),
    );
    final data = response.data['books'];
    print("Datttttaaaaaaaaaaa: $data");
    return (data as List).map((book) => BooksModel.fromJson(book)).toList();
  }

  @override
  Future<List<ChaptersModel>> getChapters(String bookSlug) async {
    final response = await dioSettings.dio.get(
      "https://www.hadithapi.com/api/$bookSlug/chapters",
      queryParameters: {
        "apiKey": ApiKeys.HADITH_API,
      },
      options: Options(
        headers: {'Content-Type': 'application/json'},
      ),
    );
    final data = response.data['chapters'];
    return (data as List)
        .map((chapter) => ChaptersModel.fromJson(chapter))
        .toList();
  }

  @override
  Future<List<HadithsModel>> getHadithsByChapterAndBook(
      String chapter, String bookSlug) async {
    final response = await dioSettings.dio.get(
      "https://www.hadithapi.com/api/hadiths/",
      queryParameters: {
        "apiKey": ApiKeys.HADITH_API,
        "book": bookSlug,
        "chapter": chapter,
      },
      options: Options(
        headers: {'Content-Type': 'application/json'},
      ),
    );
    final data = response.data['hadiths']['data'];

    return (data as List)
        .map((chapter) => HadithsModel.fromJson(chapter))
        .toList();
  }
}
