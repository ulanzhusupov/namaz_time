import 'dart:convert';

import 'package:namaz_time/core/constant/app_consts.dart';
import 'package:namaz_time/core/error/exception.dart';
import 'package:namaz_time/data/models/books_model.dart';
import 'package:namaz_time/data/models/chapters_model.dart';
import 'package:namaz_time/data/models/hadiths_model.dart';
import 'package:shared_preferences/shared_preferences.dart';

abstract class HadithLocalDataSource {
  Future<List<BooksModel>> getLastBookFromCache();
  void booksToCache(List<BooksModel> books);
  Future<List<ChaptersModel>> getChaptersFromCache();
  void chaptersToCache(List<ChaptersModel> chapters);
  Future<List<HadithsModel>> getHadithsFromCache(
      String bookSlug, String chapter);
  void hadithsToCache(
      List<HadithsModel> chapters, String bookSlug, String chapter);
}

class HadithLocalDataSourceImpl implements HadithLocalDataSource {
  final SharedPreferences sharedPreferences;

  HadithLocalDataSourceImpl({required this.sharedPreferences});

  @override
  Future<List<BooksModel>> getLastBookFromCache() async {
    final jsonBooksList =
        sharedPreferences.getStringList(AppConsts.CACHED_BOOKS);
    if (jsonBooksList?.isNotEmpty ?? false) {
      return Future.value(jsonBooksList
          ?.map((elem) => BooksModel.fromJson(jsonDecode(elem)))
          .toList());
    } else {
      throw CacheException();
    }
  }

  @override
  void booksToCache(List<BooksModel> books) {
    listDataToCache(books, AppConsts.CACHED_BOOKS);
    print("Writin booooooooooooks to cache............");
  }

  void listDataToCache(List data, String key) {
    final List<String> jsonDataList =
        data.map((elem) => jsonEncode(elem.toJson())).toList();

    sharedPreferences.setStringList(key, jsonDataList);
    print("Writin booooooooooooks to cache............");
  }

  @override
  void chaptersToCache(List<ChaptersModel> chapters) {
    listDataToCache(chapters, AppConsts.CACHED_CHAPTERS);
  }

  @override
  Future<List<ChaptersModel>> getChaptersFromCache() async {
    final jsonChaptersList =
        sharedPreferences.getStringList(AppConsts.CACHED_BOOKS);
    if (jsonChaptersList?.isNotEmpty ?? false) {
      return Future.value(jsonChaptersList
          ?.map((elem) => ChaptersModel.fromJson(jsonDecode(elem)))
          .toList());
    } else {
      throw CacheException();
    }
  }

  @override
  void hadithsToCache(
      List<HadithsModel> chapters, String bookSlug, String chapter) {
    listDataToCache(
        chapters, "${AppConsts.CACHED_HADITHS}_${bookSlug}_$chapter");
  }

  @override
  Future<List<HadithsModel>> getHadithsFromCache(
      String bookSlug, String chapter) async {
    final jsonHadithsList = sharedPreferences
        .getStringList("${AppConsts.CACHED_HADITHS}_${bookSlug}_$chapter");
    if (jsonHadithsList?.isNotEmpty ?? false) {
      return Future.value(jsonHadithsList
          ?.map((elem) => HadithsModel.fromJson(jsonDecode(elem)))
          .toList());
    } else {
      throw CacheException();
    }
  }
}
