import 'package:dartz/dartz.dart';
import 'package:namaz_time/core/error/exception.dart';
import 'package:namaz_time/core/error/failure.dart';
import 'package:namaz_time/core/platform/location_info.dart';
import 'package:namaz_time/core/platform/network_info.dart';
import 'package:namaz_time/data/data_source/location_name_remote_datasource.dart';
import 'package:namaz_time/domain/entities/location_name_entity.dart';
import 'package:namaz_time/domain/repositories/location_name_repository.dart';

class LocationNameRepositoryImpl implements LocationNameRepository {
  final LocationNameRemoteDataSource remoteDataSource;
  final NetworkInfo networkInfo;

  LocationNameRepositoryImpl({
    required this.remoteDataSource,
    required this.networkInfo,
  });

  @override
  Future<Either<Failure, LocationNameEntity>> getLocationName(
    LocationInfo locationInfo,
  ) async {
    try {
      final remoteTimes = await remoteDataSource.getLocationName(locationInfo);
      return Right(remoteTimes);
    } on ServerException {
      return Left(ServerFailure());
    }
  }
}
