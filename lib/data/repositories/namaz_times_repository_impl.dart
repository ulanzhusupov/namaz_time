import 'package:dartz/dartz.dart';
import 'package:namaz_time/core/error/exception.dart';
import 'package:namaz_time/core/error/failure.dart';
import 'package:namaz_time/core/platform/location_info.dart';
import 'package:namaz_time/core/platform/network_info.dart';
import 'package:namaz_time/data/data_source/namaz_times_local_data_source.dart';
import 'package:namaz_time/data/data_source/namaz_times_remote_data_source.dart';
import 'package:namaz_time/domain/entities/namaz_times_entity.dart';
import 'package:namaz_time/domain/repositories/namaz_times_repository.dart';

class NamazTimesRepositoryImpl implements NamazTimesRepository {
  final NamazTimesRemoteDataSource remoteDataSource;
  final NamazTimesLocalDataSource localDataSource;

  final NetworkInfo networkInfo;

  NamazTimesRepositoryImpl({
    required this.remoteDataSource,
    required this.localDataSource,
    required this.networkInfo,
  });

  @override
  Future<Either<Failure, List<NamazTimesEntity>>> getTimesForMonth(
    int year,
    int month,
    LocationInfo locationInfo,
  ) async {
    if (await networkInfo.isConnected) {
      try {
        final remoteTimes =
            await remoteDataSource.getTimesForMonth(year, month, locationInfo);
        localDataSource.namazTimesToCache(remoteTimes);
        return Right(remoteTimes);
      } on ServerException {
        return Left(ServerFailure());
      }
    } else {
      try {
        final localPerson = await localDataSource.getLastNamazTimesFromCache();
        return Right(localPerson);
      } on CacheException {
        return Left(CacheFailure());
      }
    }
  }
}
