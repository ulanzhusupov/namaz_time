import 'package:namaz_time/core/error/exception.dart';
import 'package:namaz_time/core/error/failure.dart';
import 'package:namaz_time/core/platform/network_info.dart';
import 'package:namaz_time/data/data_source/hadith_local_datasource.dart';
import 'package:namaz_time/data/data_source/hadith_remote_data_source.dart';
import 'package:namaz_time/domain/entities/books_entity.dart';
import 'package:namaz_time/domain/entities/chapters_entity.dart';
import 'package:namaz_time/domain/entities/hadiths_entity.dart';
import 'package:namaz_time/domain/repositories/hadith_repository.dart';

class HadithRepositoryImpl extends HadithsRepository {
  final HadithRemoteDataSource remoteDataSource;
  final HadithLocalDataSource localDataSource;

  final NetworkInfo networkInfo;

  HadithRepositoryImpl(
      {required this.networkInfo,
      required this.localDataSource,
      required this.remoteDataSource});

  @override
  Future<List<BooksEntity>> getBooks() async {
    if (await networkInfo.isConnected) {
      try {
        final books = await remoteDataSource.getBook();
        localDataSource.booksToCache(books);
        return books;
      } catch (e) {
        print("Ошииибка: ${e.toString()}");
        return [];
      }
    } else {
      try {
        final localBooks = await localDataSource.getLastBookFromCache();
        return localBooks;
      } on CacheException {
        throw CacheFailure();
      }
    }
  }

  @override
  Future<List<ChaptersEntity>> getChapters(String bookSlug) async {
    if (await networkInfo.isConnected) {
      try {
        final chapters = await remoteDataSource.getChapters(bookSlug);
        localDataSource.chaptersToCache(chapters);
        return chapters;
      } catch (e) {
        print("Ошииибка: ${e.toString()}");
        return [];
      }
    } else {
      try {
        final localChapters = await localDataSource.getChaptersFromCache();
        return localChapters;
      } on CacheException {
        throw CacheFailure();
      }
    }
  }

  @override
  Future<List<HadithsEntity>> getHadithsByChapterAndBook(
      String chapter, String bookSlug) async {
    if (await networkInfo.isConnected) {
      try {
        final hadiths = await remoteDataSource.getHadithsByChapterAndBook(
            chapter, bookSlug);
        localDataSource.hadithsToCache(hadiths, bookSlug, chapter);
        return hadiths;
      } catch (e) {
        print("Ошииибка: ${e.toString()}");
        return [];
      }
    } else {
      try {
        final localHadiths =
            await localDataSource.getHadithsFromCache(bookSlug, chapter);
        return localHadiths;
      } on CacheException {
        throw CacheFailure();
      }
    }
  }
}
