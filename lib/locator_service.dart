import 'package:get_it/get_it.dart';
import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:namaz_time/core/network/dio_settings.dart';
import 'package:namaz_time/core/platform/network_info.dart';
import 'package:namaz_time/data/data_source/hadith_local_datasource.dart';
import 'package:namaz_time/data/data_source/hadith_remote_data_source.dart';
import 'package:namaz_time/data/data_source/location_name_remote_datasource.dart';
import 'package:namaz_time/data/data_source/namaz_times_local_data_source.dart';
import 'package:namaz_time/data/data_source/namaz_times_remote_data_source.dart';
import 'package:namaz_time/data/repositories/hadith_repository.dart';
import 'package:namaz_time/data/repositories/location_name_repository.dart';
import 'package:namaz_time/data/repositories/namaz_times_repository_impl.dart';
import 'package:namaz_time/domain/repositories/hadith_repository.dart';
import 'package:namaz_time/domain/repositories/location_name_repository.dart';
import 'package:namaz_time/domain/repositories/namaz_times_repository.dart';
import 'package:namaz_time/domain/usecase/get_location_name.dart';
import 'package:namaz_time/domain/usecase/get_times_for_month.dart';
import 'package:namaz_time/domain/usecase/hadith_usecase.dart';
import 'package:namaz_time/presentation/bloc/hadith_cubit/hadith_cubit.dart';
import 'package:namaz_time/presentation/bloc/location_name_cubit/location_name_cubit.dart';
import 'package:namaz_time/presentation/bloc/namaz_time_bloc/namaz_times_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

final sl = GetIt.instance;

Future<void> init() async {
  // BLOC/CUBIT
  sl.registerFactory(() => LocationNameCubit(locationNameUseCase: sl()));
  sl.registerFactory(() => HadithCubit(sl()));
  sl.registerFactory(() => NamazTimesBloc(getTimesForMonth: sl()));

  // USECASES
  sl.registerLazySingleton(() => HadithUsecase(hadithsRepository: sl()));
  sl.registerLazySingleton(() => GetLocationName(locationNameRepository: sl()));
  sl.registerLazySingleton(() => GetTimesForMonth(namazTimesRepository: sl()));

  // REPOSITORY / location name
  sl.registerLazySingleton<LocationNameRemoteDataSource>(
    () => LocationNameRemoteDataSourceImpl(dioSettings: sl()),
  );
  // hadith books
  sl.registerLazySingleton<HadithRemoteDataSource>(
      () => HadithRemoteDataSourceImpl(dioSettings: sl()));
  sl.registerLazySingleton<HadithLocalDataSource>(
      () => HadithLocalDataSourceImpl(sharedPreferences: sl()));
  // namaz time
  sl.registerLazySingleton<NamazTimesRemoteDataSource>(
      () => NamazTimesRemoteDataSourceImpl(dioSettings: sl()));

  sl.registerLazySingleton<NamazTimesLocalDataSource>(
      () => NamazTimesLocalDataSourceImpl(sharedPreferences: sl()));

  // location name
  sl.registerLazySingleton<LocationNameRepository>(
    () => LocationNameRepositoryImpl(
      remoteDataSource: sl(),
      networkInfo: sl(),
    ),
  );

  // hadith books
  sl.registerLazySingleton<HadithsRepository>(
    () => HadithRepositoryImpl(
        remoteDataSource: sl(), localDataSource: sl(), networkInfo: sl()),
  );
  // namaz time
  sl.registerLazySingleton<NamazTimesRepository>(
    () => NamazTimesRepositoryImpl(
      remoteDataSource: sl(),
      localDataSource: sl(),
      networkInfo: sl(),
    ),
  );

  // Core
  sl.registerLazySingleton<NetworkInfo>(
      () => NetworkInfoImpl(connectionChecker: sl()));

  // External
  final sharedPref = await SharedPreferences.getInstance();
  sl.registerLazySingleton(() => sharedPref);
  sl.registerLazySingleton<DioSettings>(() => DioSettings());
  sl.registerLazySingleton(() => InternetConnectionChecker());
}
